import React, { useState, useEffect } from 'react';
import socket from 'socket.io-client'

import { SafeAreaView, Alert, ScrollView, View, Image, Text, TouchableOpacity, AsyncStorage, StyleSheet } from 'react-native';
import logo from '../assets/logo.png'

import SpotList from '../components/SpotList'

const List = ({ navigation }) => {

    const [techs, setTechs] = useState([])

    useEffect(() => {
        AsyncStorage.getItem('user_id').then(user_id => {
            const io = socket('https://aircnc--backend.herokuapp.com', {
                query: { user_id }
            })

            io.on('booking_res', booking => {
                Alert.alert(`Sua reserva na ${booking._spot.company} em ${booking.date} foi ${booking.approved ? 'APROVADA' : 'REJEITADA'}`)
            })
        })
    }, [])

    useEffect(() => {
        AsyncStorage.getItem('techs').then(colTechs => {
            const techsArray = colTechs.split(',').map(t => t.trim())
            setTechs(techsArray)
        })
    }, [])

    async function sair() {

        await AsyncStorage.removeItem('user_id');
        await AsyncStorage.removeItem('techs');

        navigation.navigate('Login')
    }

    return (
        <SafeAreaView style={styles.container}>
            <Image style={styles.logo} source={logo} />

            <ScrollView>
                {techs.map((t, i) => <SpotList tech={t} key={i} />)}
            </ScrollView>

            <TouchableOpacity style={styles.sair} onPress={sair} >
                <Text>Sair</Text>
            </TouchableOpacity>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    logo: {
        height: 32,
        resizeMode: 'contain',
        alignSelf: 'center',
        marginTop: 10
    },
    sair: {
        height: 32,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#d5d5d5',
    }
})

export default List
