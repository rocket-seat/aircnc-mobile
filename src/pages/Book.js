import React, { useState, useEffect } from 'react';

import { SafeAreaView, AsyncStorage, Alert, View, StyleSheet, TouchableOpacity, TextInput, Text } from 'react-native';
import api from '../services/api'

const Book = (props) => {
    const id = props.navigation.getParam('id')

    const [date, setDate] = useState('')

    async function handleSubmit() {
        const user_id = await AsyncStorage.getItem('user_id')

        await api.post('/spots/' + id + '/bookings', { date }, { headers: { user_id } })
        Alert.alert('Sua solicitação foi enviada')
        props.navigation.navigate('List')
    }

    function handleCancel() {
        props.navigation.navigate('List')
    }

    return (
        <SafeAreaView style={styles.container}>
            <Text style={styles.label}>Data de interesse</Text>
            <TextInput style={styles.input} placeholder="Data de interesse" placeholderTextColor="#999" autoCapitalize="words" autoCorrect={false} value={date} onChangeText={t => setDate(t)} />

            <TouchableOpacity onPress={handleSubmit} style={styles.button}>
                <Text style={styles.buttonText}>Solicitar reserva</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={handleCancel} style={[styles.button, styles.cancelButton]}>
                <Text style={styles.buttonText}>Cancelar</Text>
            </TouchableOpacity>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        margin: 30,
    },

    label: {
        marginTop: 30,
        fontWeight: 'bold',
        color: '#444',
        marginBottom: 8,
    },
    input: {
        borderWidth: 1,
        borderColor: '#ddd',
        paddingHorizontal: 20,
        fontSize: 16,
        color: '#444',
        height: 44,
        marginBottom: 20,
        borderRadius: 2
    },
    button: {
        height: 42,
        backgroundColor: '#f05a5b',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 2
    },
    cancelButton: {
        backgroundColor: '#ccc',
        marginTop: 10,
    },
    buttonText: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 16
    }
})

export default Book
