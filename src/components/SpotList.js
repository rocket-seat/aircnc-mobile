import React, { useState, useEffect } from 'react'
import { View, Image, Text, FlatList, StyleSheet, TouchableOpacity } from 'react-native'
import { withNavigation } from 'react-navigation'

import api from '../services/api'

const SpotList = (props) => {

    const [spots, setSpots] = useState([])

    useEffect(() => {
        async function loadSpots() {
            const res = await api.get('/spots', { params: { tech: props.tech } })
            setSpots(res.data)
        }

        loadSpots();
    }, [])

    function handleNavigate(item) {
        props.navigation.navigate('Book', { id: item._id })
    }

    function renderSpot(item) {
        return (
            <View style={styles.listItem}>
                <Image style={styles.thumbnail} source={{ uri: item.thumbnail_url }} />
                <Text style={styles.company}>{item.company}</Text>
                <Text style={styles.price}>{item.price ? `R$ ${item.price}` : 'GRATUITO'}</Text>

                <TouchableOpacity style={styles.button} onPress={() => handleNavigate(item)}>
                    <Text style={styles.buttonText}>Solicitar reserva</Text>
                </TouchableOpacity>
            </View>
        )
    }

    return (
        <View style={styles.container}>
            <Text style={styles.title}>
                Empresas que utilizam <Text style={styles.bold}>{props.tech}</Text>
            </Text>

            <FlatList style={styles.list} data={spots} keyExtractor={k => k._id} horizontal showsHorizontalScrollIndicator={false} renderItem={({ item }) => renderSpot(item)} />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        marginTop: 30,
    },
    title: {
        fontSize: 20,
        color: '#444',
        paddingHorizontal: 20,
        marginBottom: 15,
    },
    bold: {
        fontWeight: 'bold'
    },
    list: {
        paddingHorizontal: 20,
    },
    listItem: {
        marginRight: 15,
    },
    thumbnail: {
        width: 200,
        height: 120,
        resizeMode: 'cover',
        borderRadius: 2,
    },
    company: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#333',
        marginTop: 10
    },
    price: {
        fontSize: 15,
        color: '#999',
        marginTop: 5
    },
    button: {
        height: 32,
        backgroundColor: '#f05a5b',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 2,
        marginTop: 15
    },
    buttonText: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 15
    }
})

export default withNavigation(SpotList)